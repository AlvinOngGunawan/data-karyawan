package KaryawanData.ServerParking;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetSocketAddress;
import java.sql.SQLException;

public class Server extends WebSocketServer {
    public Server( int port ) {
        super( new InetSocketAddress( port ) );
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake ) {
        System.out.println( conn.getRemoteSocketAddress().getAddress().getHostAddress() + " entered the Website!" );
    }

    @Override
    public void onClose( WebSocket conn, int code, String reason, boolean remote ) {
        broadcast( conn + " has entered the Webstie!" );
        System.out.println( conn + " has entered the Website!" );
    }

    @Override
    public void onMessage( WebSocket conn, String message ) {
        Karyawan karyawan = new Karyawan();

        try {
            karyawan.query(message);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        System.out.println(message);
    }


    public static void main( String[] args ) throws InterruptedException , IOException {
        int port = 8080;
        try {
            port = Integer.parseInt( args[ 0 ] );
        } catch ( Exception ex ) {
        }
        Server s = new Server( port );
        s.start();
        System.out.println( "ServerKaryawan port =  " + s.getPort() );

        BufferedReader sysin = new BufferedReader( new InputStreamReader( System.in ) );
        while ( true ) {
            String in = sysin.readLine();
            s.broadcast( in );
            if( in.equals( "exit" ) ) {
                s.stop(1000);
                break;
            }
        }
    }
    @Override
    public void onError( WebSocket conn, Exception ex ) {
        ex.printStackTrace();
        if( conn != null ) {
        }
    }

    @Override
    public void onStart() {
        System.out.println("Server Jalan!");
        setConnectionLostTimeout(0);
        setConnectionLostTimeout(100);
    }
}
